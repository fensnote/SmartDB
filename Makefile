#通用makefile  wuquan-1230@163.com fensjoy
#文件目录
OBJ_DIR=./obj
SRC_DIR=.
INC_DIR:=.   ./include  ./include/rapidjson


SRC=$(wildcard $(SRC_DIR)/*.cpp )
OBJS+=$(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC))



#目标文件名
TARGET=testSmartDb

CPU?=X86


CFLAGS += -std=c++11 

#修改编译器
ARCH ?= 
CC=$(ARCH)gcc
CPP=$(ARCH)g++
AR=$(ARCH)ar
STRIP=$(ARCH)strip

LDFLAGS += -lpthread  -ldl  -lsqlite3 

ifeq ($(DEB), 1)
CFLAGS += -Wall -g # -finline-functions -O2 -fno-strict-aliasing
else
CFLAGS += -Wall   -finline-functions -O2 -fno-strict-aliasing 
endif

#LIB_PATH=../release/libs
#LIB_PATH=../release/static
CFLAGS  += $(foreach dir,$(INC_DIR),-I$(dir))
LDFLAGS += $(foreach lib,$(LIB_PATH),-L$(lib))

all:$(TARGET) 
$(TARGET): $(OBJS)		
	$(CPP)  $(OBJS) -o $(TARGET)  $(LDFLAGS) $(CFLAGS)
ifneq ($(DEB), 1)
	$(STRIP) $(TARGET)
endif	
	
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cc
	$(CPP)  -c $< -o $@ $(CFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CPP)  -c $< -o $@ $(CFLAGS) 

print:
	@echo "$(OBJS)"
	@echo "$(LIB_NAME)"
	@echo "$(LDFLAGS)"
	@echo "$(CFLAGS)"
	@echo "install dir: $(BIN_INSTALL)"
	@echo "bak: $(BIN_BAK)"

clean:
	-rm $(OBJS)  $(TARGET)

install:
	cp $(TARGET) $(BIN_INSTALL)/
	cp $(TARGET) $(BIN_BAK)/
